import { FETCH_CHICKEN, UPDATE_PRODUCTS, UPDATE_CART } from './types';
import foodApi from '../apis';

export const fetchChicken = () => {
  return async function (dispatch) {
    const response = await foodApi.get('/products');
    dispatch({
      type: FETCH_CHICKEN,
      payload: response.data,
    });
  };
};

export const updateItem = (id, formValues) => {
  return async function (dispatch) {
    const response = await foodApi.patch(`/products/${id}`, formValues);
    dispatch({
      type: UPDATE_PRODUCTS,
      payload: response.data,
    });
  };
};

export const editChicken = (id, formValues) => {
  return async function (dispatch) {
    await dispatch(updateItem(id, formValues));
    await dispatch(addToCart());
  };
};

export const addToCart = () => {
  return async function (dispatch, getStore) {
    const { chicken } = getStore().products;
    const result = Object.values(chicken).filter((items) => items.skus[0].inCartQuantity);
    dispatch({
      type: UPDATE_CART,
      payload: result,
    });
  };
};
