import _ from 'lodash';

export const removeFromCart = (item) => {
  const product = { ...item };
  const { skus } = _.pick(product, 'skus');
  skus[0].inCartQuantity -= 1;

  return { id: product.id, skus };
};
export const addToCart = (item) => {
  const product = { ...item };
  const { skus } = _.pick(product, 'skus');
  skus[0].inCartQuantity += 1;
  return { id: product.id, skus };
};
