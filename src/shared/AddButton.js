import React from 'react';
import { addToCart } from './CounterFunctions';

const Add = ({ item, editTheChicken }) => {
  const addCart = () => {
    const { id, skus } = addToCart(item);
    editTheChicken(id, { skus });
  };
  return (
    <button type='button' className='btn btn-primary' onClick={() => addCart()}>
      ADD
    </button>
  );
};

export default Add;
