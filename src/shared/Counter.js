import React from 'react';
import { addToCart, removeFromCart } from './CounterFunctions';

const Counter = ({ item, editTheChicken }) => {
  const addCart = () => {
    const { id, skus } = addToCart(item);
    editTheChicken(id, { skus });
  };
  const removeCart = () => {
    const { id, skus } = removeFromCart(item);
    editTheChicken(id, { skus });
  };
  return (
    <React.Fragment>
      <button type='button' className='btn btn-primary' onClick={() => removeCart()}>
        -
      </button>
      <button type='button' className='btn btn-transparent' disabled>
        {item.skus[0].inCartQuantity}
      </button>
      <button type='button' className='btn btn-success' onClick={() => addCart()}>
        +
      </button>
    </React.Fragment>
  );
};

export default Counter;
