import React, { Component } from 'react';
import { Link, NavLink } from 'react-router-dom';
import { connect } from 'react-redux';

class Navbar extends Component {
  render() {
    const { cart } = this.props;
    return (
      <nav className='navbar navbar-light bg-light'>
        <div className='container-fluid'>
          <NavLink to='/'>Chicken Menu</NavLink>
          <div>
            <Link to='/cart' type='button' className='btn btn-primary'>
              <i className='fa fa-shopping-cart' aria-hidden='true' />
              <span className='badge badge-dark'>{renderCount(cart)}</span>
            </Link>
          </div>
        </div>
      </nav>
    );
  }
}
function renderCount(cart) {
  return cart.reduce((accum, current) => {
    return accum + current.skus[0].inCartQuantity;
  }, 0);
}

const mapStateToProps = (state) => {
  return {
    cart: state.products.cart,
  };
};

export default connect(mapStateToProps, null)(Navbar);
