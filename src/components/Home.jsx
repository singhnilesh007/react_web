import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchChicken, editChicken } from '../actions';
import Add from '../shared/AddButton';
import Counter from '../shared/Counter';
class Home extends Component {
  componentDidMount = () => {
    this.props.fetchChicken();
  };

  rendercard = (item) => {
    return (
      <div className='card m-1' style={{ width: '18rem' }} key={item.id}>
        <img
          className='card-img-top'
          src={item.productImage}
          alt={item.productName}
          style={{ objectFit: 'cover', height: 200 }}
        />
        <div className='card-body'>
          <h5 className='card-title'>{item.productName}</h5>
          <p className='card-text'>{item.skus[0].quantity}</p>
          <p className='card-text'>₹ {item.skus[0].retailPrice}</p>
          {item.skus[0].inCartQuantity === 0 ? (
            <Add addToCart={this.addToCart} editTheChicken={this.editTheChicken} item={item} />
          ) : (
            <Counter
              count={item.skus[0].inCartQuantity}
              addToCart={this.addToCart}
              item={item}
              editTheChicken={this.editTheChicken}
              removeFromCart={this.removeFromCart}
            />
          )}
        </div>
      </div>
    );
  };

  editTheChicken = (id, item) => {
    this.props.editChicken(id, item);
  };
  render() {
    return (
      <div className='mb-5'>
        <h4>Chicken Menu</h4>

        <div style={{ display: 'flex', flexWrap: 'wrap', flexDirection: 'row' }}>
          {this.props.chicken.map((item) => {
            return this.rendercard(item);
          })}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    chicken: Object.values(state.products.chicken),
  };
};

export default connect(mapStateToProps, { fetchChicken, editChicken })(Home);
