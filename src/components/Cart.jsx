import React, { Component } from 'react';
import { connect } from 'react-redux';
import Add from '../shared/AddButton';
import { editChicken } from '../actions';
import Counter from '../shared/Counter';

class Cart extends Component {
  editTheChicken = (id, item) => {
    this.props.editChicken(id, item);
  };
  renderItem = (item) => {
    return (
      <div className='card' key={item.productName}>
        <div className='card-body p-2' style={{ display: 'flex', flexDirection: 'row' }}>
          <div className='w-20'>
            <img src={item.productImage} style={{ width: 74, height: 74 }} alt={item.productName} />
          </div>
          <div className='w-80 pl-2' style={{ lineHeight: 1 }}>
            <p>{item.productName}</p>
            <p>{item.skus[0].quantity}</p>
            <p> ₹ {item.skus[0].retailPrice}</p>
            {item.skus[0].inCartQuantity === 0 ? (
              <Add editTheChicken={this.editTheChicken} item={item} />
            ) : (
              <Counter item={item} editTheChicken={this.editTheChicken} />
            )}
          </div>
        </div>
      </div>
    );
  };
  render() {
    const { cart } = this.props;
    return (
      <div className='container m-5'>
        <h4>Cart</h4>
        {cart.length === 0 && <h2>There are no items in Cart</h2>}
        <div className='container'>
          <div className='row'>
            <div className='col-sm'>
              {cart.map((item) => {
                return this.renderItem(item);
              })}
            </div>
            {cart.length !== 0 && (
              <div className='col-sm'>
                <div className='card'>
                  <div className='card-body p-2'>Total Price is: ₹ {renderTotalPrice(cart)}</div>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

function renderTotalPrice(cart) {
  return cart.reduce((accum, current) => {
    return (accum + current.skus[0].retailPrice) * current.skus[0].inCartQuantity;
  }, 0);
}

const mapStateToProps = (state) => {
  return {
    cart: state.products.cart,
  };
};

export default connect(mapStateToProps, { editChicken })(Cart);
