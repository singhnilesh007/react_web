import { UPDATE_PRODUCTS, FETCH_CHICKEN, UPDATE_CART } from '../actions/types';
import _ from 'lodash';

const INITIAL_STATE = {
  chicken: [],
  cart: [],
};

export const chickenReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case FETCH_CHICKEN:
      const chicken = { ..._.mapKeys(action.payload, 'id') };
      return { ...state, chicken: chicken };
    case UPDATE_PRODUCTS:
      return {
        ...state,
        chicken: {
          ...state.chicken,
          [action.payload.id]: action.payload,
        },
      };
    case UPDATE_CART:
      return {
        ...state,
        cart: action.payload,
      };

    default:
      return state;
  }
};
