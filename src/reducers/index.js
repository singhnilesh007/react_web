import { combineReducers } from 'redux';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { chickenReducer } from './chickenReducer';

const persistConfig = {
  key: 'root',
  storage: storage,
};

const rootReducers = combineReducers({
  products: chickenReducer,
});

export default persistReducer(persistConfig, rootReducers);
